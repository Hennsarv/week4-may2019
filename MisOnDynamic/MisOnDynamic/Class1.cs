﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MisOnDynamic
{
    public class Program
    {

        class PropBag : DynamicObject
        {
            public Dictionary<string, dynamic> prop = new Dictionary<string, dynamic>();
            public override bool TrySetMember(SetMemberBinder binder, object value)
            {
                prop[binder.Name] = value;
                return true;
            }
            public override bool TryGetMember(GetMemberBinder binder, out object result)
            {
                result = prop[binder.Name];
                return true;
            }
        }

        public static void Main()
        {
            var arv = 77L;
            string nimi = "Henn";

            arv++;
            nimi = nimi.ToUpper();

            dynamic d = 77;
            d++;
            Console.WriteLine(d);
            d = nimi;
            //d++;
            Console.WriteLine(d);


            dynamic kott = new PropBag();

            kott.omanik = "Henn";
            kott.raha = 7000;
            kott.raha *= 2;

            Console.WriteLine(kott.omanik);
            Console.WriteLine(kott.raha);

        }

    }

}
