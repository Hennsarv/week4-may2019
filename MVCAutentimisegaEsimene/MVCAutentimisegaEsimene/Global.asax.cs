﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace MVCAutentimisegaEsimene
{
    public class MvcApplication : System.Web.HttpApplication
    {
        int sessionNr = 0;

        // see meetod käivitatakse ESIMESEL rakenduse poole pöördumisel
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // siia kaks rida, et teha Decimalile oma binder
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new DecimalModelBinder());


            // pistan application dictionary, mida tahan
            Application["Nimi"] = "Seee on meie rakendus";
            Application["Start"] = DateTime.Now;

        }

        protected void Session_Start()
        {
            Session["Started"] = DateTime.Now;
            Session["Nr"] = ++sessionNr;
        }

    }
}
