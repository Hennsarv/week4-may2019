﻿using System.Web.Mvc;
using System.Globalization;

namespace MVCAutentimisegaEsimene
{
    // selle klassi lisasin juurde, et ta oskaks decimali tagasi lugeda punktiga formaadis 
    internal class DecimalModelBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            var value = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
            if (value == null)
                return null;
            if (string.IsNullOrEmpty(value.AttemptedValue))
                return null;
            try
            {
                var decimalValue = value.ConvertTo(typeof(decimal), CultureInfo.GetCultureInfo("et-ee"));
                return decimalValue;
            }
            catch
            {
                 

                if (decimal.TryParse(value.AttemptedValue,NumberStyles.AllowDecimalPoint  ,CultureInfo.InvariantCulture, out decimal decimalValue))
                return decimalValue;
                throw;
            }
        }
    }
}