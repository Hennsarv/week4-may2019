﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCAutentimisegaEsimene
{
    public class AuthorizeUsersAttribute : AuthorizeAttribute
    {
        public string RedirectUrl { get; set; } = "";


        public AuthorizeUsersAttribute()
            : base()
        {
        }

        public AuthorizeUsersAttribute(string redirectUrl)
            : base()
        {
            this.RedirectUrl = redirectUrl;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            // kui on vaja, et autentimata kasutaja saadetakse alati sisse logima
            // siis korja järgmise rea eest kommentaar maha!
            //if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                string authUrl = this.RedirectUrl; //passed from attribute NotifyUrl Property

                //if null, get it from config
                if (String.IsNullOrEmpty(authUrl))
                    // suunamise URL loetakse WebConfigist nimega - vt webConfigis on kommentaar
                    authUrl = System.Web.Configuration.WebConfigurationManager.AppSettings["RolesAuthRedirectUrl"];

                if (!String.IsNullOrEmpty(authUrl))
                    filterContext.HttpContext.Response.Redirect(authUrl);
            }

            //else do normal process
            base.HandleUnauthorizedRequest(filterContext);
        }
    }
}