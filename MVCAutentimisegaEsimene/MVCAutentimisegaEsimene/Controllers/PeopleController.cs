﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisegaEsimene.Models;
using System.Globalization;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;

namespace MVCAutentimisegaEsimene.Controllers
{
    public class Riik       // tegin klassi Riik - kohe seletan, miks sellise
    {
        public string name { get; set; }  // nimed ei vasta reeglitele - vaatame, miks see nii
        public string alpha2Code { get; set; }
    }

    public class PeopleController : Controller
    {
        static List<string> Riigid // algul tegin riikide loetelu käsitsi
        =>     
            new List<string>
        {
            "Eesti", "Läti", "Saksa", "Rootsi"
        };

        static List<Riik> Riigid2    // siin proovisin riikide loetelu lugeda veebist
        {
            get
            {
                // leidsin aadressi, kust saab lugeda riikide loendi json-formaadis
                // sellliseid aadresse nimetatakse ka rest-teenusteks - neist hiljem
                WebRequest wr = WebRequest.Create("https://restcountries.eu/rest/v2/all");
                HttpWebResponse resp = (HttpWebResponse)wr.GetResponse();
                Stream str = resp.GetResponseStream();
                StreamReader sr = new StreamReader(str);
                string vastus = sr.ReadToEnd();
                // siin sellline jada (ise ka ei team leidsin miski asja googlist)
                // 1. teen WebRequest objecti (parameetriks veebiaadress)
                // 2. küsin sellelt vastuse (selle käigus tehakse http-request)
                // 3. vastusest erladan vastuse voo (striimi) - decodeeritud baidijada (body)
                // 4. ehitan Streamreaderi, mis seda jada loeb
                // 5. loen ta korraga stringi
                // NB! selle jaoks on vaja kaks usingut System.Net ja System.IO

                // kuna sealt tulev vastus on json kujul, siis lisan oma rakendusele json teegi
                // install-package Newtonsoft.Json - korra sellest rääkisime

                // kasutan Jsoni lahtilugejat (deserializer), kes teeb mul loetust List<Riik>
                // NB! seal jsonis on muid atribuute veel - aga kui minu Riik-klassis neid pole
                // siis lahtilugeja jätab nad vahele (ignoreerib)
                return JsonConvert.DeserializeObject<List<Riik>>(vastus);
            }
        }

        private Entities db = new Entities();

        // GET: People
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }

        // GET: People/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // GET: People/Create
        public ActionResult Create()
        {
            AspNetUser u = db.AspNetUsers.Where(x => x.Email == User.Identity.Name).SingleOrDefault();

            Person person = new Person
            {
                EMail = User.Identity.Name,
                BirthDate = u.BirthDate ?? (new System.DateTime(2000, 1, 1)),
                FirstName = u.DisplayName.Split(' ')[0],
                LastName = (u.DisplayName + " ").Split(' ')[1],
                Nationality = "Estonia"
            };

            ViewBag.Nationality = new SelectList(
            Riigid2.Select(x => new { Nationality = x.name, Name = x.name }).ToList()
            , "Nationality", "Name", person.Nationality)
            ;

            return View(person);  // create viewle antakse sageli ette "eeltäidetud" object
        }

        // POST: People/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.People.Add(person);
                db.SaveChanges();
                Session["Person"] = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();

                return RedirectToAction("Index", "Home");
            }


            // NB! Samasugune asi on ka Create meetodi esimeses pooles
            // enamasti pannakse see SelectList Viewle seljakotti (ViewBag - hiljem räägime sellest veel)
            // sama nimega, mis hakkab olema väljal (vormis) ja ka sealt tagasi tuleval objektil
            ViewBag.Nationality = 
                new SelectList( // select list on vahend, millega saab hõlpsast ehitada dopdowni
                                // see koosneb ühest listist, kahest stringist ja vaikeväärtusest
                Riigid2.Select(x => new { Nationality = x.name, Name = x.name }).ToList(),
                                // listi elementidel on vähemalt kaks elementi - väärtus ja nimi
                                // praegu panen need samaks
                "Nationality",  // väärtusatribuudi nimi
                "Name",         // nimeatribuudi nimi
                person.Nationality)    // vaikimisiväärtus, et oleks võimlaik kaas anda (võib ka puududa)
            ;

            return View(person);
        }

        // GET: People/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,FirstName,LastName,EMail,BirthDate,Nationality")] Person person)
        {
            if (ModelState.IsValid)
            {
                db.Entry(person).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(person);
        }

        // GET: People/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Person person = db.People.Find(id);
            if (person == null)
            {
                return HttpNotFound();
            }
            return View(person);
        }

        // POST: People/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Person person = db.People.Find(id);
            db.People.Remove(person);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
