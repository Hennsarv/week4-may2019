﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisegaEsimene.Models;

namespace MVCAutentimisegaEsimene.Controllers
{
    public class HomeController : MyController
    {
        public ActionResult Index()
        {
            CheckPerson();
            ViewBag.Ylemus = RequestBag.Ylemus;
            ViewBag.Person = SessionBag.Person;
            ViewBag.R = RequestBag;
            if(!Request.IsAuthenticated) return View();
            else
            {
                Entities db = new Entities();
                Person p = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();
                if (p == null) return RedirectToAction("Create", "People");
                return View();



            }
        }

        
        // Authorize atribuudi asemele tegin uue atribuudi AuthorizeUsers
        // selle jaoks on vastav klass tehtud (vaata - kas F12 või otsi üles)
        // see atribuut on muidu TÄPSELT nagu Authorize kui lisandub võimalus
        // lisada suunamisURL
        [AuthorizeUsers(RedirectUrl = "/Categories/Index")]
        public ActionResult About()
        {
            Entities db = new Entities();

            ViewBag.Message = "Your application description page.";
            ViewBag.Rollid = db.AspNetRoles
                .Where(x => x.AspNetUsers.Select(y => y.Email).Contains(User.Identity.Name))
                .Select(x => x.Name)
                .ToList();

            return View();
        }

        [AuthorizeUsers] // ka siin on uus atribuut, aga suunamisurl võtekase WebConfigist
                         // ja kui seal ei ole, siis tehakse nagu vanasti 
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            ViewBag.HennuTeade = "Henn kirjutas selle kontrolleris";
            ViewData["teineteade"] = "väga imelik asi";

            ViewBag.SessionNr = SessionBag.Nr;
            ViewBag.SessionStarted = SessionBag.Started;

            return View();
        }
    }
}