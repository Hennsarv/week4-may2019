﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisegaEsimene.Models;

namespace MVCAutentimisegaEsimene.Controllers
{
    public class MyController : Controller
    {
        protected Person Person;

        protected dynamic TempBag => new UniversalBag(TempData);
        protected dynamic ApplicationBag => new UniversalBag(System.Web.HttpContext.Current.Application);
        protected dynamic SessionBag => new UniversalBag(System.Web.HttpContext.Current.Session);
        protected dynamic RequestBag => new UniversalBag(Request);

        protected void CheckPerson()
        {
            using (Entities db = new Entities())
            {
                if (Request.IsAuthenticated)
                    if (SessionBag.Person == null)
                        SessionBag.Person = Person = db.People.Where(x => x.EMail == User.Identity.Name).SingleOrDefault();
                    else { Person = SessionBag.Person; }
                else SessionBag.Person = null;
            }
        }
    }
}