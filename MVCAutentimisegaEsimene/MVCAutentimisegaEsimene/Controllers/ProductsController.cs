﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisegaEsimene.Models;

namespace MVCAutentimisegaEsimene.Controllers
{
    public class ProductsController : MyController
    {
        private Entities db = new Entities();

        // GET: Products
        public ActionResult Index(string sort = "ProductId", string direction = "ascending", int page = 1, int pageSize = 10)
        {
            CheckPerson();
            ViewBag.PersonName = Person?.FirstName;
            var pageCount = db.Products.Count() / pageSize + 1;
            var products = db.Products.Include(p => p.Category);
            

            ViewBag.Sort = sort;
            ViewBag.Direction = direction;
            ViewBag.Page = page;
            ViewBag.Pages = pageCount;
            ViewBag.PageSize = pageSize;
            
            products =
                sort == "ProductId" ? products.OrderBy(x => x.ProductID) :
                sort == "CategoryId" ? products.OrderBy(x => x.CategoryID) :
                sort == "UnitPrice" ? products.OrderBy(x => x.UnitPrice) :
                sort == "ProductName" ? products.OrderBy(x => x.ProductName) :
                products;

            return View(
                direction == "descending"
                ? products.AsEnumerable().Reverse().Skip(pageSize*(page-1)).Take(pageSize).ToList()
                : products.Skip(pageSize * (page - 1)).Take(pageSize).ToList()
                );

        }

        // GET: Products/Details/5
        [AuthorizeUsers(RedirectUrl = "/Categories/Index")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create(int? id)    // lisasin parameetri id, mis viitab kategooriale, kuhu toode läheb
                                               // kui id == null siis tuleb kategooria valida
                                               // id.hasvalue järgi otsustame, kas pöörduti Product.List või Category.Details lehelt
        {
            Session["ret"] = id.HasValue ? "Categeories" : "Products";      // määrame, kuhu lehele vaja tagasi pöörduda

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", id);

            return View(new Product { CategoryID = id }); // viewle saadame juba valmis tehtud Producti, kus CategoryId olemas
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                if (Session["ret"].Equals("Products"))   // vastavalt sessioonimuutjale pöördume Products või Categories lehele
                    return RedirectToAction("Index");
                return RedirectToAction("Details", "Categories", new { id = product.CategoryID });
            }

            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id, string ret = "Products")  // kuna siin on id alati olema, lisame parameetri ret - kuhu minn atagasi
        {
            Session["ret"] = ret;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            ViewBag.Ret = ret;
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductID,ProductName,SupplierID,CategoryID,QuantityPerUnit,UnitPrice,UnitsInStock,UnitsOnOrder,ReorderLevel,Discontinued")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Entry(product).State = EntityState.Modified;
                db.SaveChanges();
                if (Session["ret"].Equals("Categories")) return RedirectToAction("Details", "Categories", new { id = product.CategoryID });
                return RedirectToAction("Index");
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "CategoryID", "CategoryName", product.CategoryID);
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id, string ret)  // sama lugu, mis Edit meetodis
        {
            Session["ret"] = ret;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {

            Product product = db.Products.Find(id);
            int productId = product?.CategoryID ?? 0; // NB! tagasi suunatav Category/Details/id - categoryid tuleb meelde
                                                      // jätta ENNE kustutamist (peale seda on juba hilja)
            db.Products.Remove(product);
            db.SaveChanges();
            if (Session["ret"].Equals("Categories")) return RedirectToAction("Details", "Categories", new { id = productId });
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
