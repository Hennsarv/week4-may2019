﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCAutentimisegaEsimene.Models;

namespace MVCAutentimisegaEsimene.Models
{
    partial class AspNetUser
    {
        public string RoleId { get; set; }
        public string RoleName => AspNetRoles.FirstOrDefault()?.Name;
    }

}

namespace MVCAutentimisegaEsimene.Controllers
{
    public class UsersController : Controller
    {
        public List<string> GetAllCountries()
        {
            return CultureInfo.GetCultures(CultureTypes.AllCultures)
                .Select(x => x.EnglishName)
                .ToList();
        }


        private Entities db = new Entities();

        // GET: Users
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
                return View(db.AspNetUsers.ToList());
            else
                return RedirectToAction("Index", "Home");
        }

        // GET: Users/Details/5
        public ActionResult Details(string id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }

            
            // võtame rollide hulgast ainult need, mille kasutajate hulgas ei ole
            // konkreetset kasutajat (aspNetUser)
            ViewBag.Roles = db.AspNetRoles
              .Where(r => ! r.AspNetUsers.Select(x => x.Email).Contains(aspNetUser.Email))
              .ToList();

            return View(aspNetUser);
        }

        public ActionResult AddRole(string id, string role)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            if (u != null) db.RemoveRoles(u.Id);
            db.SaveChanges();
            AspNetRole r = db.AspNetRoles.Find(role);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Add(r);
                    db.SaveChanges();
                }
                catch { }
            }

            return RedirectToAction("Details", new { id });
        }

        public ActionResult RemoveRole(string id, string role)
        {
            AspNetUser u = db.AspNetUsers.Find(id);
            AspNetRole r = db.AspNetRoles.Find(role);
            if (u != null && r != null)
            {
                try
                {
                    u.AspNetRoles.Remove(r);
                    db.SaveChanges();
                }
                catch { }
            }

            return RedirectToAction("Details", new { id });
        }

        // GET: Users/Create
        public ActionResult Create()
        {
            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name");
            return View();
        }

        // POST: Users/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,DisplayName,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,RoleId")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                AspNetRole role = db.AspNetRoles.Find(aspNetUser.RoleId);
                db.AspNetUsers.Add(aspNetUser);
                db.SaveChanges();
                if(role != null)
                {
                    aspNetUser.AspNetRoles.Add(role);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name");
            return View(aspNetUser);
        }

        // GET: Users/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.Countries = GetAllCountries();
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            aspNetUser.RoleId = aspNetUser.AspNetRoles.FirstOrDefault()?.Id;
            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name", aspNetUser.RoleId);
            return View(aspNetUser);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,DisplayName,Email,EmailConfirmed,PasswordHash,SecurityStamp,PhoneNumber,PhoneNumberConfirmed,TwoFactorEnabled,LockoutEndDateUtc,LockoutEnabled,AccessFailedCount,UserName,RoleId")] AspNetUser aspNetUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(aspNetUser).State = EntityState.Modified;
                db.SaveChanges();
                db.RemoveRoles(aspNetUser.Id);
                db.SaveChanges();
                AspNetRole role = db.AspNetRoles.Find(aspNetUser.RoleId);
                if(role != null)
                {
                    aspNetUser.AspNetRoles.Add(role);
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            aspNetUser.RoleId = aspNetUser.AspNetRoles.FirstOrDefault()?.Id;
            ViewBag.RoleId = new SelectList(db.AspNetRoles, "Id", "Name", aspNetUser.RoleId);

            return View(aspNetUser);
        }

        // GET: Users/Delete/5
        public ActionResult Delete(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            if (aspNetUser == null)
            {
                return HttpNotFound();
            }
            return View(aspNetUser);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            AspNetUser aspNetUser = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(aspNetUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
